Rails.application.routes.draw do
  devise_for :users

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'

  resources :cities
  resources :events
  resources :topics
  resources :user_filters

  root 'welcome#index'
end
