# Preview all emails at http://localhost:3000/rails/mailers/notification_mailer
class NotificationMailerPreview < ActionMailer::Preview
  def event_mailer
    NotificationMailer.event_mailer(User.first, Event.first)
  end
end
