# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(email: 'test1@test.com', password: 'password', password_confirmation: 'password', name: 'User 1')
User.create!(email: 'test2@test.com', password: 'password', password_confirmation: 'password', name: 'User 2')

city1 = City.create!(name: 'Москва')
city2 = City.create!(name: 'Санкт-Петербург')

topic1 = Topic.create!(name: 'Тема 1')
topic2 = Topic.create!(name: 'Тема 2')

event1 = Event.new(name: 'Событие 1', city_id: city1.id, start_date: 1.hour.from_now, end_date: 5.hour.from_now, location: 'Метро 1')
event2 = Event.new(name: 'Событие 2', city_id: city2.id, start_date: 6.hour.from_now, end_date: 10.hour.from_now, location: 'Метро 2')

event1.events_topics.build(topic: topic1)
event2.events_topics.build(topic: topic2)

event1.save!
event2.save!



