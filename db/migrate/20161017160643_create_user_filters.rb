class CreateUserFilters < ActiveRecord::Migration[5.0]
  def change
    create_table :user_filters do |t|
      t.references :user, null: false, index: true, foreign_key: true
      t.references :city, index: true, foreign_key: true
      t.references :topic, index: true, foreign_key: true
      t.datetime :start_period
      t.datetime :end_period
      t.timestamps
    end
  end
end
