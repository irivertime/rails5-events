class CreateEventsTopics < ActiveRecord::Migration[5.0]
  def change
    create_table :events_topics do |t|
      t.references :event, null: false, index: true, foreign_key: true
      t.references :topic, null: false, index: true, foreign_key: true
    end
  end
end
