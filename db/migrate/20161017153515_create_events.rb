class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :name, null: false
      t.references :city, null: false, index: true, foreign_key: true
      t.datetime :start_date, null: false
      t.datetime :end_date, null: false
      t.string :location, null: false
      t.timestamps
    end
  end
end
