class CreateJournals < ActiveRecord::Migration[5.0]
  def change
    create_table :journals do |t|
      t.references :user, null: false, index: true, foreign_key: true
      t.references :event, null: false, index: true, foreign_key: true
      t.boolean :unread, default: true
      t.timestamps
    end
  end
end
