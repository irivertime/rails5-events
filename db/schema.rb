# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161017213915) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cities", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "name",       null: false
    t.integer  "city_id",    null: false
    t.datetime "start_date", null: false
    t.datetime "end_date",   null: false
    t.string   "location",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_events_on_city_id", using: :btree
  end

  create_table "events_topics", force: :cascade do |t|
    t.integer "event_id", null: false
    t.integer "topic_id", null: false
    t.index ["event_id"], name: "index_events_topics_on_event_id", using: :btree
    t.index ["topic_id"], name: "index_events_topics_on_topic_id", using: :btree
  end

  create_table "journals", force: :cascade do |t|
    t.integer  "user_id",                   null: false
    t.integer  "event_id",                  null: false
    t.boolean  "unread",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["event_id"], name: "index_journals_on_event_id", using: :btree
    t.index ["user_id"], name: "index_journals_on_user_id", using: :btree
  end

  create_table "topics", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_filters", force: :cascade do |t|
    t.integer  "user_id",      null: false
    t.integer  "city_id"
    t.integer  "topic_id"
    t.datetime "start_period"
    t.datetime "end_period"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["city_id"], name: "index_user_filters_on_city_id", using: :btree
    t.index ["topic_id"], name: "index_user_filters_on_topic_id", using: :btree
    t.index ["user_id"], name: "index_user_filters_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",               null: false
    t.string   "encrypted_password",     default: "",               null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,                null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "name",                   default: "Anonymous User", null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.boolean  "is_online"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "events", "cities"
  add_foreign_key "events_topics", "events"
  add_foreign_key "events_topics", "topics"
  add_foreign_key "journals", "events"
  add_foreign_key "journals", "users"
  add_foreign_key "user_filters", "cities"
  add_foreign_key "user_filters", "topics"
  add_foreign_key "user_filters", "users"
end
