# README

This README would normally document whatever steps are necessary to get the
application up and running.

Rails 5
ActionCable
PostgreSQL
Puma
Sidekiq

## Dependencies:

*   ruby >= 2.3.x
*   redis >= 2.8.x
*   PostgreSQL

## Configuration

```bash
cp config/database.example.yml config/database.yml
bundle install
rails db:create
rails db:migrate
rails db:seed
```

## Running

```bash
rails s
```

## Run services job queues

```bash
bundle exec sidekiq
```

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
