class NotificationsChannel < ApplicationCable::Channel
  def subscribed
    stream_for current_user
  end

  def read_all_notices
    current_user.read_all_notices
  end
end  
