json.extract! event, :id, :name, :city_id, :start_date, :end_date, :location, :created_at, :updated_at
json.url event_url(event, format: :json)