class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery

  before_action :authenticate_user!

  before_action :notification_data, if: :current_user

  protected

  def notification_data
    @notifications = current_user.journals.unread
  end
end
