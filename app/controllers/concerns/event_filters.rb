module EventFilters
  extend ActiveSupport::Concern

  included do
    before_action :remember_filter, only: [:index]
    before_action :restore_filter, only: [:index]
  end

  def remember_filter
    return unless params.has_key?(:remember_filter)
    filter = current_user.user_filter ||= current_user.build_user_filter
    filter.update_attributes(city_id: params[:by_city], topic_id: params[:by_topic],
                             start_period: params[:q].try(:[], :start_date_gteq),
                             end_period: params[:q].try(:[], :start_date_lteq))
  end

  def restore_filter
    return unless params.has_key?(:restore_filter)
    filter = current_user.user_filter
    params[:by_city] = filter.city_id
    params[:by_topic] = filter.topic_id
    params[:q][:start_date_gteq] = filter.start_period.strftime("%d.%m.%Y %H:%M")
    params[:q][:start_date_lteq] = filter.end_period.strftime("%d.%m.%Y %H:%M")
  end
end