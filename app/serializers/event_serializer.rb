class EventSerializer < ActiveModel::Serializer
  attributes :id, :name, :url

  def url
    event_path(object)
  end
end

