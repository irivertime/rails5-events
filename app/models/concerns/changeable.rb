module Changeable
  extend ActiveSupport::Concern

  def appear
    update_attribute(:is_online, true)
  end

  def away
    update_attribute(:is_online, false)
  end

  def read_all_notices
    journals.update_all(unread: false)
  end
end
