class Event < ApplicationRecord
  include Observers::EventObserver

  belongs_to :city
  has_many :events_topics, dependent: :destroy
  has_many :topics, through: :events_topics

  scope :by_city, ->(city_id) { where(city_id: city_id) }
  scope :by_topic, ->(topic_id) { joins(:events_topics).where(events_topics: { topic_id: topic_id } ) }

  def to_s
    name
  end
end
