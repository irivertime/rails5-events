class UserFilter < ApplicationRecord
  belongs_to :user

  scope :by_city, ->(city_id) { where(city_id: city_id) }
  scope :by_topic, ->(topic_ids) { where(topic_id: topic_ids) }
  scope :by_period, ->(start_date) { where('start_period <= ? AND end_period >= ?', start_date, start_date) }

  scope :for, ->(city_id, topic_ids, start_date) { by_city(city_id).by_topic(topic_ids).by_period(start_date) }
end
