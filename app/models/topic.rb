class Topic < ApplicationRecord
  has_many :events_topics, dependent: :destroy
  has_many :events, through: :events_topics

  def to_s
    name
  end
end
