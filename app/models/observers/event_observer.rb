module Observers
  module EventObserver
    extend ActiveSupport::Concern

    included do
      after_save :send_notifications
    end

    def send_notifications
      filters = UserFilter.for(city_id, topic_ids, start_date)

      filters.map(&:user).each do |user|
        NotificationBroadcastJob.perform_later(user, self)
        NotificationMailer.event_mailer(user, self).deliver_later
      end
    end
  end
end
