class Journal < ApplicationRecord
  belongs_to :user
  belongs_to :event

  scope :unread, -> { where(unread: true) }
end
