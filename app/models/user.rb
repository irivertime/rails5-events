class User < ApplicationRecord
  include Changeable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :journals, dependent: :destroy
  has_one :user_filter

  def to_s
    name
  end
end
