class NotificationBroadcastJob < ApplicationJob
  queue_as :default

  def perform(user, event)
    if user.is_online?
      NotificationsChannel.broadcast_to(
        user,
        render_notification(event)
      )
    end
    Journal.create(user: user, event: event, unread: !user.is_online?)
  end

  private

  def render_notification(event)
    ApplicationController.render(partial: 'shared/notification_item', locals: { event: event })
  end
end