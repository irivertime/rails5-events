class NotificationMailer < ApplicationMailer
  default from: "from@example.com"

  def event_mailer(user, event)
    @user = user
    @event = event
    mail(to: @user.email, subject: "Notification about new event: #{@event.name}")
  end
end
