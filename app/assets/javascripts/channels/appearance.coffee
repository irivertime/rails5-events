App.cable.subscriptions.create "AppearanceChannel",
  connected: ->
    @appear()

  disconnected: ->
    @away()

  rejected: ->
    @away()

  appear: ->
    @perform("appear")

  away: ->
    @perform("away")
