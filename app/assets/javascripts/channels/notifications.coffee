App.cable.subscriptions.create "NotificationsChannel",
  connected: ->
    @binding()

  disconnected: ->
    @unbinding()

  received: (data) ->
    $('.notifications').append(data)
    @changeCounter()

  initCounter: () ->
    $('.count-notifications').html(0)

  changeCounter: () ->
    $('.count-notifications').html(parseInt($('.count-notifications').html()) + 1)

  binding: ->
    $(document).on "shown.bs.dropdown", ".dropdown-notifications", =>
      @read_all_notices()
      @initCounter()
      false

  unbinding: ->
    $(document).off(".dropdown-notifications")

  read_all_notices: ->
    @perform("read_all_notices")